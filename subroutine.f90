!
!DRAFT
!to compile: f2py -c --fcompiler=gnu95 -m QC subroutine.f90
subroutine empty_files(station_data,EMONTH_py,no_years,skip)
!to identify station files with only missing values.
 integer    :: month,yr,EMONTH,skip,no_data,data_points
 integer,intent(in)     :: EMONTH_py,no_years
 real,intent(in),dimension(:,:)    :: station_data
 real, parameter           :: MISSING = -99.9 
 !f2py intent(out)     :: skip
 
 EMONTH = EMONTH_py+1
 skip=0
 no_data=0
 data_points=0

 do yr = 1,no_years
!  do yr = 1,size(station_data,1)
    do month = 2,EMONTH
        if (station_data(yr,month)<= MISSING) no_data = no_data + 1
        data_points = data_points + 1
    enddo
 enddo
 if (no_data == data_points) skip = 1

end subroutine empty_files


 
subroutine negative_values(station_data,EMONTH_py,no_years,flag,year)
!To locate negative value where there should not be negative values. Such as for precipitation and counts of days indices.
IMPLICIT NONE
 integer                :: month,yr,EMONTH,flag,year
 integer,intent(in)     :: EMONTH_py,no_years
 real,intent(in),dimension(:,:)    :: station_data
 real, parameter           :: MISSING = -99.9
 
 !f2py intent(out)     :: flag,year
 
 flag = 0
 EMONTH = EMONTH_py+1 !Fortran starts counting from 1; Python from 0.
 
 do yr=1,no_years
        do month=2,EMONTH ! month by month
                if (station_data(yr,month)<= MISSING)CYCLE !skip missing data
                if(station_data(yr,month).lt.0 .and. station_data(yr,month)/=MISSING) then !test for negative values.
!                 if(station_data(yr,month).lt.0) then !use this to test -99.9 as a negative value.
                        flag = 1 !ERROR!
                        year = int(station_data(yr,1))
                        return
                endif
        enddo
 enddo
 return
end subroutine negative_values



subroutine percentile(station_data,EMONTH_py,min_no_yrs,index_50p,num_years,flag,average_base)
!To test that annual values average 10% during the base period 1961-1990 for percentile indices.
IMPLICIT NONE
 real,intent(in),dimension(:,:)    :: station_data
 real, parameter           :: MISSING = -99.9
 integer,intent(in)     ::  min_no_yrs,EMONTH_py,index_50p,num_years
 integer                :: yr,datayears,flag,yr_1961,yr_1990,EMONTH
 real                   :: tot_base,average_base
 
 !f2py intent(out)     :: flag,average_base
 
 flag = 0 
 EMONTH = EMONTH_py+1 
 
 tot_base = 0
 datayears = 0 !check if this needs to start at 0 or 1.
 yr_1961 = 1

 !Find 1961:
 IF ((int(station_data(1,1)) > 1961) .and. (int(station_data(1,1)) <1990)) THEN
    yr_1990 = 1990 - int(station_data(yr_1961,1))
 ELSEIF (int(station_data(1,1)) < 1961) THEN
     do while ((station_data(yr_1961,1)/=1961) .and. (yr_1961 <= num_years))
!      do while ((station_data(yr_1961,1)/=1961) .and. (yr_1961 <= size(station_data,1)))
         yr_1961=yr_1961+1
     enddo
     yr_1990 = yr_1961+29
 ELSEIF (int(station_data(1,1)) == 1961)THEN
     yr_1990 = 30
 ELSEIF (int(station_data(1,1)) >= 1990) THEN 
    flag=1
    RETURN !A percentile should not have been calculated. exit with error flagged.
 ENDIF

 do yr=yr_1961,yr_1990 !Loop through base period.
     if (station_data(yr,EMONTH) > MISSING) then
         datayears = datayears + 1      !The number of years without missing data
         tot_base = station_data(yr,EMONTH) + tot_base        !Total value. Used to calculate average.
     endif
 enddo

 if (datayears >= min_no_yrs) THEN !Does it meet threshold?
     average_base = tot_base/datayears    !calculate average annual value during base period.
     if ((index_50p == 0) .and. ((average_base > 10.5) .or. (average_base < 9.5))) flag = 1  !Is 9.5< average value >10.5?
     if ((index_50p == 1) .and. ((average_base > 51.00) .or. (average_base < 49.00))) flag = 1 !TX50p,TN50p
     RETURN
 ELSEIF ((datayears < min_no_yrs) .and. (datayears /= 0)) THEN !not enough years to calculate percentile. specify threshold in QC_main.py
     flag = 1 ! ERROR!
     average_base = MISSING
     RETURN
 ENDIF
end subroutine percentile




SUBROUTINE annual_exists(station_data,num_months,num_years,flag,year)
!To test that there is no annual value when a monthly value is missing.
IMPLICIT NONE
 real,intent(in),dimension(:,:)    :: station_data
 real, parameter           :: MISSING = -99.9
 integer                :: yr,flag, month,year,NE_month,data_month
 integer, intent(in)    :: num_months,num_years
 !f2py intent(out)     :: flag,year
 
 flag = 0

 do yr=1,num_years
!  do yr=1,size(station_data,1)
     NE_month = 0
     if (station_data(yr,14) > MISSING) THEN !Annual value identified.
         data_month = 0
         do month=2,13
             IF (station_data(yr,14) /= station_data(yr,month)) NE_month = NE_month + 1
             if (station_data(yr,month) > MISSING) data_month = data_month + 1
         enddo
         if (NE_month == 12) THEN
             flag = 1 !annual value does not equal a monthly value
             year = int(station_data(yr,1))
             RETURN
         ENDIF
         if (data_month < num_months) THEN
            flag = 1
            year = int(station_data(yr,1)) !Monthly value absent so annual value is not valid.
            return
         endif
     endif
 enddo
 
END SUBROUTINE annual_exists



SUBROUTINE map_2points(lon0, lat0, lon1, lat1,station_distance,distance_km,nearby)
!Create a list of nearby stations. Geodesic distance is calculated using a great circle and radius of the Earth.
IMPLICIT NONE
 integer,parameter      :: r_earth = 6378137 !Earth equatorial radius in meters according to the World Geodetic System.
 real,intent(in)     ::lon0, lat0, lon1, lat1,station_distance
 double precision    ::coslt1,sinlt1,coslt0,sinlt0,cosl0l1,cosc,distance,k !better accuracy
 REAL, PARAMETER     :: Pi = 3.1415927
 integer             :: nearby
 real                :: distance_km
 !f2py intent(out)   :: distance_km,nearby

 k = Pi/180.0
 nearby = 0

 coslt1 = cos(k*lat1)
 sinlt1 = sin(k*lat1)
 coslt0 = cos(k*lat0)
 sinlt0 = sin(k*lat0)
 cosl0l1 = cos(k*(lon1-lon0))
 cosc = sinlt0 * sinlt1 + coslt0 * coslt1 * cosl0l1 !Cos of angle between pnts
 
 distance= acos(cosc) * r_earth
 distance_km = real(distance/10000)
!  print*,distance_km
 if (distance_km < station_distance) nearby = 1
         
END SUBROUTINE Map_2points

SUBROUTINE compare_indices(station_data,station_data1,EMONTH_py,no_yrs,no_yrs1,flag,year)
!Compare if one index has higher/lower values than another. eg. is TXx<TNn, Rx1day<Rx5day?
IMPLICIT NONE
 real,intent(in),dimension(:,:)    :: station_data,station_data1
 real, parameter           :: MISSING = -99.9
 integer                   :: yr,year,flag,EMONTH,month,no_years,SYEAR,yrs 
 integer,intent(in)         :: EMONTH_py,no_yrs,no_yrs1
 !f2py intent(out)         :: year,flag   

 EMONTH = EMONTH_py+1
 flag = 0
 SYEAR = 1
 
 if (no_yrs <= no_yrs1) THEN
    no_years = no_yrs
 ELSEIF (no_yrs > no_yrs1) THEN
    no_years = no_yrs1
 ENDIF
    

 if (station_data(1,1) == station_data1(1,1)) THEN
     DO yr = SYEAR,no_years
        do month = 2,EMONTH
         if ((station_data(yr,month) <= MISSING) .OR. (station_data1(yr,month) <= MISSING)) CYCLE !Skip missing data
         if (station_data(yr,month) < station_data1(yr,month)) THEN
             flag = 1
             year = int(station_data(yr,1))
             return !return to python. error found.
         endif
        enddo
     enddo
     
 ELSEIF (station_data(1,1) < station_data1(1,1)) THEN !check we are comparing the correct years.
    do while (station_data(SYEAR,1) < station_data1(1,1))
        SYEAR = SYEAR+1
    enddo
    yr=0
    do yrs = SYEAR,no_years
        yr = yr + 1
        do month = 2,EMONTH
         if ((station_data(yrs,month) <= MISSING) .OR. (station_data1(yr,month) <= MISSING)) CYCLE !Skip missing data
         if (station_data(yrs,month) < station_data1(yr,month)) THEN
             flag = 1
             year = int(station_data(yr,1))
             RETURN
         endif
       enddo
    enddo
    
 ELSEIF (station_data(1,1) > station_data1(1,1)) THEN
    do while (station_data(1,1) > station_data1(SYEAR,1))
        SYEAR = SYEAR+1
    enddo
    yr=0
    do yrs = SYEAR,no_years
        yr = yr + 1
        do month = 2,EMONTH
         if ((station_data(yr,month) <= MISSING) .OR. (station_data1(yrs,month) <= MISSING)) CYCLE !Skip missing data
         if (station_data(yr,month) < station_data1(yrs,month)) THEN
             flag = 1
             year = int(station_data(yr,1))
             RETURN
         endif
       enddo
    enddo
 endif
 
END SUBROUTINE compare_indices

SUBROUTINE repeats(station_data,num_years,duplicate,year1,year2)
!Do years/months repeat within a station?
IMPLICIT NONE
real,intent(in),dimension(:,:)  :: station_data
integer                   :: duplicate,line,line_2,no_data,month,data_points,year1,year2
real, parameter           :: MISSING = -99.9
integer, intent(in)       :: num_years
!f2py intent (out)        :: duplicate,year1,year2

 duplicate=0
 DO line = 1,num_years
!  DO line = 1,size(station_data,1) 
    no_data=0
    do month = 2,14 !what about the first column of data?
        if (station_data(line,month)<= MISSING) no_data = no_data + 1
    enddo
    if (no_data == 12) CYCLE !skip years with missing values

    DO line_2 = 2,num_years-1
!     DO line_2 = 2,size(station_data,1)-1
        data_points = 0
        if (line == line_2) CYCLE
        DO month = 2,14
            !to find identical monthly values that are not missing
            if ((station_data(line,month) == station_data(line_2,month)) .and. (station_data(line,month)> MISSING)) THEN 
                data_points = data_points + 1
            ENDIF
        ENDDO
        if (data_points >= 10) THEN !two years with at least 10 common monthly values
            duplicate = 1
            year1 = int(station_data(line,1))
            year2 = int(station_data(line_2,1))
            RETURN
        ENDIF
    enddo
 enddo
end subroutine repeats

SUBROUTINE record_test(station_data,index_record,num_years,test4min,columns,exceed,year,world_record)
!To test the exceedance of world records
IMPLICIT NONE
real,intent(in),dimension(:,:)  :: station_data
integer,intent(in)              :: num_years,test4min,columns
real,intent(in)                 :: index_record
integer                         :: yr,year,exceed,EMONTH
real                            :: world_record
real, parameter                 :: MISSING = -99.9
 !f2py intent (out)        ::  exceed,year,world_record
 EMONTH = columns + 1 !Python starts counting at zero, Fortran at 1.

 exceed = 0 !flag
 if (test4min == 1) THEN !is value GT world record?
    do yr = 1,num_years
        if (station_data(yr,EMONTH) > index_record) THEN
            exceed = 1
            year = int(station_data(yr,1))
            RETURN
        endif
    enddo
 elseif (test4min == 0) THEN
    do yr = 1,num_years !is value LT world record?
        if ((station_data(yr,EMONTH) > MISSING) .and. (station_data(yr,EMONTH) < index_record)) THEN
            exceed = 1
            year = int(station_data(yr,1))
            world_record = station_data(yr,EMONTH)
            RETURN
        endif
    enddo
 endif
END SUBROUTINE record_test