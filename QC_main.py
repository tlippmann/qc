#!/usr/bin/python

'''
DRAFT
This script is to test and create the main program to test quality control of HadEX2.
to compile: f2py -c --fcompiler=gnu95 -m QC subroutine.f90

to debug insert pdb.set_trace() at the point where the porgram should top. interactive python shell will commence.

Tanya Lippmann 2013
t.lippmann@unsw.edu.au
'''

import os,time,datetime,pdb,csv,operator, stat
import numpy as np
import QC as f
from sets import Set as set
from collections import defaultdict
#from itertools import tee, islice, chain, izip

#class QC(object):
   #def __init__(self):

#indices=["TXn"]
indices = ["CDD","CSDI","CWD","FD","GSL","ID","PRCPTOT","R10mm","R20mm","R95p","R99p","Rnnmm","SDII","SU","TR","WSDI","DTR","Rx1day","Rx5day","TN10p","TN50p","TN90p","TNn","TNx","TX10p","TX50p","TX90p","TXn","TXx","R99pTOT","R95pTOT"] #For loop uses this list. See main program below.

  #if __name__=='__main__':
	
opt_dir = './log_files_'+str(datetime.date.today())+'/'  # output directory: If it does not exist, it will be created.
if not os.path.exists(opt_dir):
	os.makedirs(opt_dir)
	
HadEX2_dir = '/srv/ccrc/data08/z3271138/HadEX2_stationdata/'
Templates_dir = HadEX2_dir+'Templates_csv_HadEX2.1/'
missing_threshold = 0.85   #Set the missing threshold. Usual range is 0.7-0.85
station_distance = 5  #units=km. Print out of stations within ##kms of each other.



#SPECIFY THE INDICES RELEVANT FOR EACH test:
#Are these indices calculated annually or monthly?
monthly_indices = set(["DTR","Rx1day","Rx5day","TN10p","TN50p","TN90p","TNn","TNx","TX10p","TX50p","TX90p","TXn","TXx"])
annual_indices = set(["CDD","CSDI","CWD","FD","GSL","ID","PRCPTOT","R10mm","R20mm","R95p","R99p","Rnnmm","SDII","SU","TR","WSDI","R95pTOT","R99pTOT"])

GT_index = (["TNx","TXx","TXn","R10mm","R95p","PRCPTOT","Rx5day"])
LT_index = (["TNn","TNx","TNn","R20mm","R99p","R95p","Rx1day"])
#Do we need to include TXx>TNn?
#(these indices are all calculated monthly).

world_records = {'TXx':56.7, 'TNn':-89.2, 'Rx1day':1825, 'PRCPTOT':26470}
# http://wmo.asu.edu/#continental

#THESE INDICES SHOULD NOT HAVE NEGATIVE VALUES:
neg_test = set(["R10mm","R20mm","SDII","SU","TR","WSDI","DTR","Rx1day","Rx5day","FD","ID","GSL","Rnnmm","CDD","CWD","PRCPTOT","R99pTOT","R95pTOT"])

#THESE INDICES SHOULD AVERAGE 10% over the base period 1961-1990. EXCEPT TN50p & TX50p which would average 50%:
percentile_period = set(['TX10p','TX90p','TN10p','TN90p','R95pTOT','R99pTOT','TX50p','TN50p'])
index_50p = set(['TX50p','TN50p'])



percentile_threshold = int(30 * missing_threshold) #The number of years with data required to calculate the percentile base period 1961-1990.
min_months_data = int(12*missing_threshold) #how many months do we need to have an annual value?
latest_yr = 1990 - percentile_threshold #The latest legitimate start year for a percentile index.
missing=-99.9
errors = {}
tests = {}

#To extract relevant information from csv file.	
def station_csv_info(index,row_no):
    line_sp = stations[row_no].split(',')
    station_ID = line_sp[0].strip()
    filename = station_ID.replace('"',"")    
    source = line_sp[10].strip()
    source_dir = source.replace('"',"")
    station_filename = HadEX2_dir+source_dir+'/formatted_indices/'+index+'/'+filename+'_'+index+'.txt'
    
    Lat = line_sp[1]
    Lon = line_sp[2]
    Alt = line_sp[3]
    return station_filename,station_ID,Lat,Lon


def open_stuff(index):
    template_file = open(Templates_dir+'HadEX2_station_list_all_'+index+'.csv','rb') #open station list
    stations = template_file.readlines() #read station list
    if (index in annual_indices):
            no_columns = 1 #annual index.
    elif (index in monthly_indices):
            no_columns = 13        # 13 data columns. ie. index with monthly values
    return stations,no_columns

def total_error_count(station_ID,test_word):
    #This tallies the total number of errors that get found for each station across all of its indices. This is not weighted  by the number of indices that represent a single station or the number of tests that the station undergoes. i.e. some stations are represented by all indices and undergo many tests, whereas other stations may only have rainfall indices and undergo relatively little testing.
    if station_ID not in errors:
        errors[station_ID] = 1
        tests[station_ID] = test_word
    else:
        errors[station_ID] += 1
        tests[station_ID] += test_word
    return

print '\nQuality control test beginning now: '
#MAIN PROGRAM STARTS HERE:

for index in indices:
        t1=time.clock()  #get cpu time
 	clock_t1 = time.time() #get clock time

        log = opt_dir+'QC_'+index+'.log'  #create log file(s)
        nearby_log = opt_dir + 'nearby_stations_'+index+'.log'
        empty = opt_dir + 'empty_stations_' + index + '.log'
        template = Templates_dir+'HadEX2_station_list_all_'+index+'.csv' #station list

        with open(template,'r') as template_file, open(log,'w') as log_file, open(nearby_log,'w') as nearby_stations, open(empty,'w') as empty_files:

            log_file.write('Task starts from:'+str(time.strftime('%X %x %Z'))+'\n') #write start time to log file
            stations = template_file.readlines() #read station list
            stations,columns = open_stuff(index)
 	
 	#log_file = open(opt_dir+'QC_'+index+'.log','w')  #create log file(s)
	#log_file.write('Task starts from:'+str(time.strftime('%X %x %Z'))+'\n') #write start time to log file
        #nearby_stations = open(opt_dir + 'nearby_stations_'+index+'.log','w')
        #empty_files = open(opt_dir + 'empty_stations_' + index + '.log','w')

            if (index in GT_index):
                position = GT_index.index(index)
                LT = LT_index[position]
                consistent_log = opt_dir + index + '_stations_LT_' + LT + '.log'
                consistent_indices = open(consistent_log,'w')

            seen = set()
            seen1 = set()

            for row in xrange(1,len(stations)):
                    station_filename,station_ID,lat,lon = station_csv_info(index,row)
                    seen.add(station_filename)
                    
                    try: #check if station file exists. If doesn't exist. write to log file as error and skip tests.
                            station_data = np.loadtxt(station_filename,dtype='float',skiprows=1,ndmin=2)
                    except IOError as e:
                            log_file.write('FILE DOES NOT EXIST: '+station_filename+'\n')
                            pass
                    else:
                            num_years = len(station_data[:,0]) #Length of station record in years

                            #FIND nearby stations <10km. However, these calculations are expensive and many stations are repreated between indices but some are not....
                            for row1 in xrange(2,len(stations)-1):
                                    #print 'outer row: {rowout}. inner row: {rowin} '.format(rowout=row,rowin=row1)
                                    station_filename1,ID1,lat1,lon1 = station_csv_info(index,row1)
                                    seen1.add(station_filename1)

                                    if station_filename1==station_filename:
                                        continue #Do not compare the station with itself.
                                    if station_filename1 in seen and station_filename in seen1:
                                        #pdb.set_trace()
                                        continue     #Don't compare 2 stations that have already been compared

                                    distance,nearby = f.map_2points(lon,lat,lon1,lat1,station_distance)
                                    if (nearby == 1):
                                        distance = '%.2f' % distance
                                        nearby_stations.write('Stations <{totdistance}km apart ({act_dist} km): '.format(totdistance=station_distance,act_dist=distance) + station_filename + '\n' + station_filename1 + '\n') ##write to seperate log file.
                                        #total_error_count(station_ID,'station nearby')


                            #Check if entire station is empty.
                            skip = f.empty_files(station_data,columns,num_years)
                            if (skip == 1):
                                empty_files.write(station_filename + '\n')
                                continue #skip this station. it has no data.

                            if (index in GT_index):
                                LT_filename,ID_LT,LT_lat,LT_lon = station_csv_info(LT,row)
                                try:
                                        station_data1 = np.loadtxt(LT_filename,dtype='float',skiprows=1,ndmin=2)
                                except IOError as e:
                                    pass
                                else:
                                    num_years1 = len(station_data1)
                                    flag,year = f.compare_indices(station_data,station_data1,columns,num_years,num_years1)
                                    if (flag == 1):
                                        consistent_indices.write('{index1} < {index2} in {yr} '.format(index1=index,index2=LT,yr=year) + station_filename + '\n' + LT_filename + '\n') #write to log file.
                                        total_error_count(station_ID,', inconsistent indices')

                            #WORLD RECORD exceedance test:
                            if (index in world_records) :
                                index_record = world_records[index]
                                if (index != 'TNn'):
                                        test4min = 1
                                elif (index == 'TNn'):
                                        test4min = 0
                                exceed,year,world_record = f.record_test(station_data,index_record,num_years,test4min,columns)
                                if (exceed == 1):
                                    world_record =  '%.2f' % world_record
                                    log_file.write('World record ({record}) in {yr}: '.format(record = world_record,yr = year) + station_filename + '\n')
                                    total_error_count(station_ID,', world records')

                            #Test FOR NEGATIVE VALUES:
                            if (index in neg_test):
                                flg,year = f.negative_values(station_data,columns,num_years)
                                if (flg == 1): # ERROR FOUND!
                                    log_file.write('NEGATIVE VALUE FOUND in {yr} '.format(yr=year) + station_filename +  '\n')
                                    total_error_count(station_ID,', negative value')

                            #Test THE BASE PERCENTILE PERIOD:
                            if (index in percentile_period):
                                '''Should the percentile indices be calculated for this station?!
                                i.e. how many years have data betweeen 1961-1990?'''
                                if (index in index_50p):
                                    percentile_50 = 1 #This test creates many errors! Confirm the inclusion of this test?
                                else:
                                    percentile_50 = 0
                                    
                                flg,average_base = f.percentile(station_data,columns,percentile_threshold,percentile_50,num_years)
                                average_base =  '%.2f' % average_base
                                if (flg == 1): # ERROR FOUND!
                                    log_file.write('Base period average ({average}) '.format(average=average_base) + station_filename + '\n') #Does not average 10% between 1961-1990. Or not enough years with data to calculate percentile.
                                    total_error_count(station_ID,', percentile base period')

                            if (index in monthly_indices): #Tests for monthly indices
                                duplicate,year1,year2 = f.repeats(station_data,num_years)
                                if (duplicate == 1):
                                    log_file.write('years {yr1} & {yr2} are matching '.format(yr1=year1,yr2=year2) + station_filename + '\n')
                                    total_error_count(station_ID,', repeats data')
                                    
                                #To test that annual values are only present if all 12 months have data and annual value is one of the monthly values
                                flg,year = f.annual_exists(station_data,min_months_data,num_years)
                                if (flg == 1):
                                    log_file.write('ANNUAL VALUE DOES NOT EQUAL MONTHLY in {yr} '.format(yr=year) + station_filename + '\n')
                                    total_error_count(station_ID,', annually incongruent')

            # write error summary to txt file.
            dd = defaultdict(list)
            for d in (errors,tests): #join the two lists together
                for key,value in d.iteritems():
                    dd[key].append(value)
            sorted_errors = sorted(dd.iteritems(), key=operator.itemgetter(1), reverse = True) #sorted errors by number of errors.
            

            
            if (index in GT_index):
                consistent_indices.close()
                os.system('chmod a+rwx ' + consistent_log)
            t2 = time.clock() #end cpu time
            clock_t2 = time.time() #end clock time
            clock_time = clock_t2 - clock_t1
            cpu_time = t2 - t1 #get cpu time

            log_file.write('Task ends:'+str(time.strftime('%X %x %Z'))+'\n' +
            'Clock TIME: ' + str(clock_time) + '\n' +
            'CPU TIME: '+str(cpu_time)+'\n') #write times to log file.
            
        #close log files:
        #pdb.set_trace()
        os.system('chmod a+rwx ' + log) #Change permission of log files: Read, write, and execute by others.
        os.system('chmod a+rwx ' + nearby_log)
        os.system('chmod a+rwx ' + empty)

errors_file = opt_dir + 'errors_summary.log' ### Total count of errors per station
with open(errors_file,'w') as write_errors:
    for item in sorted_errors:
        write_errors.write("{stuff}\n".format(stuff = item))
os.system('chmod a+rwx ' + errors_file)
            
print ('Finished. ' + '\n' +
        'CPU time: ' + str(cpu_time) + '\n' ) 
        #'Clock time: '+ str(clock_time/60) + ' mins')