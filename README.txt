QC
==

QC_using_f2py

TO RUN:
 QC_compile_and_run.sh
 This compiles and runs entire QC program. (both Fortran and Python components).


git: 
 repo:https://github.com/tlipp/QC
 clone: https://github.com/tlipp/QC.git
 
 
output:
 ./log_files_YYYY_MM_DD/

To debug in python, insert: 
pdb.set_trace()
The program will stop (in Python) when it reaches this point and you can use the command line to interact/proceed.
more info: http://docs.python.org/2/library/pdb.html
To run from within Python type:
import QC_main
run QC_main.py
 
contact:
 Tanya Lippmann
 t.lippmann@unsw.edu.au